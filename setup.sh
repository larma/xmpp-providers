#!/bin/sh

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: CC0-1.0

#
# This script sets up everything needed to work inside of the local repository.

cp pre-commit .git/hooks/
